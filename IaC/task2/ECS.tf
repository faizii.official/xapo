resource "aws_ecs_task_definition" "service" {
  family = "service"
  container_definitions = jsonencode([
    {
      name      = "bitcoin_core"
      image     = "dexterquazi/bitcoin:0.21.0"
      cpu       = 10
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 8332
          hostPort      = 8332
        }
      ]
    }
  ])

  volume {
    name      = "service-storage"
    host_path = "/data"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  }
}
resource "aws_iam_policy" "policy" {        #used to create policy to asume role with name "btc-prod-policy"
  name        = "btc-prod-policy"
  description = "btc-prod-policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Resource = "arn:aws:iam::${var.account_id}:role/*"
      },
    ]
  
  })
}
variable "account_id" {
  default = ""  #add account id here 
}
variable "region" {
  default = "us-west-2" # add region here 
}
variable "access_key" {
    default = "" #add access key here 
}
variable "secret_key" {
  default = "" #add secret key 
}
variable "user" {
  default = "btc-prod-user"  
}
variable "group" {
  default = "btc-prod-group" 
}

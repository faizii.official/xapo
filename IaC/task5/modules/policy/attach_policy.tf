resource "aws_iam_policy_attachment" "policy_attach" {  #used to attch policy to group
  name       = "policy_attachment"
  groups     = [var.group]
  policy_arn = aws_iam_policy.policy.arn
}


provider "aws" {        #set the provider from terrafrom variable file variable.tf
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
} 

resource "aws_iam_user" "btc-prod-user" { #used to create user with name "btc-prod-user"
  name = "btc-prod-user"

}
resource "aws_iam_access_key" "btc-prod-access_key" {
  user = aws_iam_user.btc-prod-user.name
}


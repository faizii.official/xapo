resource "aws_iam_user_group_membership" "group_attach" { #used to attach  group and user
  user = var.user

  groups = [
    var.group
  ]
}

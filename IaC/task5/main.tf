
module "user" {
  source             = "./modules/user"
}
module "group" {
  source             = "./modules/group"
}
module "role" {
  source             = "./modules/role"
}
module "policy" {
  source             = "./modules/policy"
}

module "attach_group" {
  source             = "./modules/attach_group"
}
